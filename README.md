## TF-CCTV

TF-CCTV is a [Coral](https://coral.ai) compatible [Tensorflow](https://www.tensorflow.org) model designed to process CCTV footage. Originally made for [OS-NVR](https://gitlab.com/osnvr/os-nvr)

The biggest current limitation is the large amount of inaccurately annotated images in the datasets. I estimate that 25% of the images are either completely wrong or irrelevant for this purpose, and that 90% have small inaccuracies.

The model is trained on a rented server. I have spent over $250 so far. If you want to help pay for the server costs, please donate.

<details>
<summary>XMR Address</summary>
8Bth85ei7pzUFpPST5iSgRU7HthCju38VM9r7J7VMV7jErDtZpeJTDUicEEkJwfApxKQEPiALQw8iGQ4R9WTH47dPAYvA7U
</details>

<br>

#### Drop-in Docker replacements.

[Frigate](https://hub.docker.com/r/curid/frigate_tf-cctv)

[DOODS](https://hub.docker.com/r/curid/doods_tf-cctv)

<br>

### Models

The models are based on MobileDet with additional training data added.


##### CCTV1

Proof of concept model. Can only detect `person`. Resolution changed from `320x320` to `420x280`. Achieves an improvement of 1.7mAP on gray images. Though at the cost of approximately 18% slower inference time.

<details>
<summary>CCTV2 failed run</summary>
Frigate compatible model. Can only detect `person`. Resolution changed from `320x320` to `340x340`. Approximately 18% slower inference time. Failed run, accuracy is worse than original model.
</details>

##### CCTV3

Frigate compatible model. Only trained on gray images. Can only detect `person`. Resolution changed from `320x320` to `340x340`. Achieves the same accuracy on both gray and color images. Approximately 18% slower inference time. 

<br>

### BENCHMARKS

| MODEL          | TESTSET                    | mAP  |
| -------------- | -------------------------- | ---- |
| mobiledet      | color\_coco-minival_cctv1  | 44.1 |
| mobiledet      | gray\_coco-minival_cctv1   | 38.9 |
| -              |                            |      |
| rcnn_inception | color\_coco-minival_cctv1  | 50.7 |
| rcnn_inception | gray\_coco-minival_cctv1   | 45.8 |
| -              |                            |      |
| cctv1          | color\_coco-minival_cctv1  | 43.7 |
| cctv1          | gray\_coco-minival_cctv1   | 40.6 |
| -              |                            |      |
| cctv2          | color\_coco-minival_cctv2  | 39.1 |
| cctv2          | gray\_coco-minival_cctv2   | 37   |
| -              |                            |      |
| cctv3          | color\_coco-minival_cctv3  | 39   |
| cctv3          | gray\_coco-minival_cctv3   | 39.1 |

<br>

### BASE MODELS

| SIZE    | NAME           | FULL NAME                         |
| ------- | -------------- | --------------------------------- |
| 320x320 | mobiledet      | ssdlite\_mobiledet\_edgetpu/fp32  |
| any     | rcnn_inception | faster\_rcnn\_inception\_v2\_coco |

<br>

### TESTSETS

Blacklisted images are removed from all testsets.

| IMAGES | NAME                       | DESCRIPTION                                          |
| ------ | ---------------------------|------------------------------------------------------|
| 720    | coco-minival_cctv1         | Minival color and gray, cctv1 labels.                |
| 349    | color\_coco-minival_cctv1  | Minival original, cctv1 labels. Gray images removed. |
| 371    | gray\_coco-minival_cctv1   | Minival coverted to black and white, cctv1 lables.   |

<br>

### TRAINSETS

| SIZE | NAME  | DESCRIPTION                                                              |
| ---- | ----- | -------------------------------------------------------------------------|
| 144k | cctv1 | cctv1 labels. Combination of coco2017 and voc2012. Blacklist not applied |
| 144k | cctv2 | Same as cctv1.                                                           |
| 59k  | cctv3 | cctv3 labels. coco2017 and voc2012 images coverted to gray.              |


<br>

#### Naming convention.

\<TYPE>\_\<DATASET>\_\<LABELS>

| type  | description                            |
|-------|----------------------------------------|
| color | Original images.                       |
| gray  | Images converted to black and white.   |
| empty | Combined color and gray.               |

<br>

### DATASETS

#### COCO2017

https://cocodataset.org

#### VOC2012

http://host.robots.ox.ac.uk/pascal/VOC/voc2012/

<br>

### Resources

https://coral.ai/docs/edgetpu/retrain-detection/

https://towardsdatascience.com/custom-object-detection-using-tensorflow-from-scratch-e61da2e10087

https://medium.com/mini-distill/effect-of-batch-size-on-training-dynamics-21c14f7a716e

<br>


Copyright (c) 2021, Curid, <Curid@protonmail.com>
