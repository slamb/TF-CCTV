#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")

if [ "$#" -ne 1 ]; then
	echo "Need release to build."
	exit 1
fi

release="$1"

cd "$script_dir" || exit

../utils/prepare_models.sh "$script_dir"

docker buildx build \
	--pull --push \
	-t curid/doods_tf-cctv:"$release" \
	--platform linux/amd64,linux/arm,linux/arm64 .

docker buildx build \
	--pull --push \
	-t curid/doods_tf-cctv:latest \
	--platform linux/amd64,linux/arm,linux/arm64 .
