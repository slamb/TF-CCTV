#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir" || exit

../utils/prepare_models.sh "$script_dir"

docker image build -t curid/doods_tf-cctv:local .
