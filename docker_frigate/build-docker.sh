#!/bin/sh

version="0.9.4" # Frigate version.

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir" || exit

../utils/prepare_models.sh "$script_dir"

docker image build \
	--build-arg version="$version" \
	--build-arg arch=amd64 \
	-t curid/frigate_tf-cctv:local .
