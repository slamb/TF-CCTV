#!/bin/sh

version="0.9.4" # Frigate version.
release="r1.2"  # TF-CCTV release.

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir" || exit

../utils/prepare_models.sh "$script_dir"

build_and_push() {
	arch=$1
	docker image build \
		--build-arg version="$version" \
		--build-arg arch="$arch" \
		-t curid/frigate_tf-cctv:"$version"_"$release"_"$arch" .

	docker push curid/frigate_tf-cctv:"$version"_"$release"_"$arch"
}

build_and_push amd64
build_and_push armv7
build_and_push aarch64
