#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")
home_dir="$script_dir/.."

sudo docker run -p 0.0.0.0:6006:6006 \
	-it -v "$home_dir/:/home/tensorflow/models/research/content" tf1-cctv /bin/bash
