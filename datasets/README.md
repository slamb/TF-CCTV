### mscoco_blacklist.txt

| start | end   | desciption                                       |
|-------|-------|--------------------------------------------------|
| 1     | 5380  | Crowd images, include 99.5% of all crowd images. |
| 5381  | 13952 | Person close up images.                          |
| 13953 | 17423 | Person minival images.