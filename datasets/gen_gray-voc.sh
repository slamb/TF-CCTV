#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir" || exit

mkdir -p "$script_dir/gray-VOC2012/JPEGImages"
cp -r "$script_dir/VOC2012/Annotations" "$script_dir/gray-VOC2012"

# Generate black and white version of all images.
for file in "$script_dir"/VOC2012/JPEGImages/*; do
	out=$(printf '%s' "$file" | sed -e 's|VOC2012|gray-VOC2012|')
	echo input "$file" output "$out"
	ffmpeg -y -i "$file" -vf hue=s=0 "$out" >/dev/null 2>/dev/null
done
