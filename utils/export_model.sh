#!/bin/sh

if [ "$#" -ne 1 ]; then
	echo "Need checkpoint prefix to export."
	exit 1
fi

script_dir=$(dirname "$(readlink -f "$0")")
home_dir="$script_dir/.."
tmp_dir="$home_dir/tmp"

checkpoint_path=$(realpath "$1")
checkpoint_dir=$(dirname "$checkpoint_path")

cd "$home_dir" || exit

mkdir "$tmp_dir"

python3 ../object_detection/export_inference_graph.py \
	--input_type=image_tensor \
	--pipeline_config_path="$checkpoint_dir/pipeline.config" \
	--output_directory="$tmp_dir" \
	--trained_checkpoint_prefix="$checkpoint_path"

rm -r "$checkpoint_dir/saved_model"
mv "$tmp_dir/saved_model" "$checkpoint_dir"
mv "$tmp_dir/frozen_inference_graph.pb" "$checkpoint_dir"

rm -r "$tmp_dir"
