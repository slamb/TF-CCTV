#!/bin/sh

script_dir=$1
home_dir="$script_dir"/..
models_dir="$script_dir"/models

cd "$script_dir" || exit

mkdir -p "$models_dir"/cctv1
cd "$models_dir"/cctv1 || exit
cp "$home_dir"/labels/cctv1-doods.txt ./labels.txt
cp "$home_dir"/models/cctv1/cctv1_420x280.tflite ./
cp "$home_dir"/models/cctv1/cctv1_420x280_edgetpu.tflite ./

mkdir "$models_dir"/cctv2
cd "$models_dir"/cctv2 || exit
cp "$home_dir"/labels/cctv2-doods.txt ./labels.txt
cp "$home_dir"/models/cctv2/cctv2_340x340.tflite ./
cp "$home_dir"/models/cctv2/cctv2_340x340_edgetpu.tflite ./

mkdir "$models_dir"/cctv3
cd "$models_dir"/cctv3 || exit
cp "$home_dir"/labels/cctv3-doods.txt ./labels.txt
cp "$home_dir"/models/cctv3/gray_cctv3_340x340.tflite ./
cp "$home_dir"/models/cctv3/gray_cctv3_340x340_edgetpu.tflite ./
