import os

from py_utils.create_records import create_records
from py_utils.processor import Processor
from py_utils.utils import check_sum


dir_path = os.path.dirname(os.path.realpath(__file__))
home_path = os.path.dirname(os.path.dirname(dir_path))
datasets_path = os.path.join(home_path, "datasets")

labels_path = os.path.join(home_path, "labels", "cctv3.pbtxt")

p = Processor(datasets_path, labels_path)

color_coco = p.gen_coco(1, minival=True)

create_records(color_coco, dir_path, 1)

check_sum(color_coco, dir_path)

print("images", len(color_coco["images"]))
